/*import Handsontable from 'handsontable';*/

// import { Handsontable } from 'https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js';


const data = [
    ['', 'Tesla', 'Volvo', 'Toyota', 'Ford'],
    ['2019', 10, 11, 12, 13],
    ['2020', 20, 11, 14, 13],
    ['2021', 30, 15, 12, 13]
  ];
  
  const container = document.getElementById('example');
  const hot = new Handsontable(container, {
    data: data,
    rowHeaders: true,
    colHeaders: true,
    height: 'auto',
    licenseKey: 'non-commercial-and-evaluation' // for non-commercial use only
  });